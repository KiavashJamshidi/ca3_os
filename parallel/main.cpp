#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cmath>
#include <limits>
#include <iomanip>
#include <cstdlib>
#include <pthread.h>

using namespace std;

class Mobile {
    public:
        vector<float> details;
        float price_range;
        float min,max;
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setPrice() {
            price_range = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }

};

class Weight{
    public:
        vector<float> details;
        float bias;
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setBias() {
            bias = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }
};

void getCsvFiles(vector<Mobile> &mobiles,vector<Weight> &weights,
 char* argv[],int isWeight){
    fstream file;
    string dir = argv[1];
    string csvFile;
    string line;
    vector<string> lines;
    if (isWeight == 1) csvFile = "weights.csv";
    else csvFile = "train.csv";
    file.open(dir + string(csvFile));

    while(getline(file,line))
        lines.push_back(line);

    file.close();

    for (int each=1;each<lines.size();each++) {
        Weight w;
        Mobile m;
        vector<string> v;
        stringstream ss(lines[each]);

        while (ss.good()) {
            string substr;
            getline(ss, substr, ',');
            v.push_back(substr);
        }
        for (size_t i = 0; i < v.size(); i++)
            if (isWeight) w.addDetail(stof(v[i]));
            else m.addDetail(stof(v[i]));
        
        if (isWeight){
            w.setBias();
            weights.push_back(w);
        }
        else {
            m.setPrice();
            mobiles.push_back(m);
        }
    }
}

struct normilize_threads{
    vector<Mobile> mobiles;
    int eachCol;
};

void* normilizeThread(void* args){
    struct normilize_threads *myData;
    myData = (struct normilize_threads *) args;

    float min = myData->mobiles[0].details[myData->eachCol];
    float max = myData->mobiles[0].details[myData->eachCol];
    for (int i=1;i<myData->mobiles.size();i++){
        if (myData->mobiles[i].details[myData->eachCol] < min)
            min = myData->mobiles[i].details[myData->eachCol];
        if (myData->mobiles[i].details[myData->eachCol] > max)
            max = myData->mobiles[i].details[myData->eachCol];
    }

    for (int i=0;i<myData->mobiles.size();i++){
        float newX = (myData->mobiles[i].details[myData->eachCol] - min) / (max - min);
        myData->mobiles[i].details[myData->eachCol] = newX;
    }
    pthread_exit(NULL);
}

void normilize(vector<Mobile> &mobiles){
    int numOfCols = mobiles[0].details.size();
    // pthread_t threads[numOfCols];

    // struct normilize_threads threadData;
    // threadData.mobiles = mobiles;

    for (int eachCol=0;eachCol < numOfCols;eachCol++){
        // threadData.eachCol = eachCol;
        // pthread_create(&threads[eachCol],NULL,normilizeThread,(void *) &threadData);
        // mobiles = threadData.mobiles;
        float min = numeric_limits<float>::max();
        float max = numeric_limits<float>::min();
        for (int i=0;i<mobiles.size();i++){
            if (mobiles[i].details[eachCol] < min)
                min = mobiles[i].details[eachCol];
            if (mobiles[i].details[eachCol] > max)
                max = mobiles[i].details[eachCol];
        }

        for (int i=0;i<mobiles.size();i++){
            float newX = (mobiles[i].details[eachCol] - min) / (max - min);
            mobiles[i].details[eachCol] = newX;
        }
    }
}

struct thread_data {
    vector<Weight> weights;
    vector<Mobile> mobiles;
    long i;
    float accuracy;
};

void* calculateThread(void* threadArgs){
    struct thread_data *myData;
    myData = (struct thread_data *) threadArgs;
    float max = numeric_limits<float>::min();
    int whichIndex;
    for (int w=0;w<myData->weights.size();w++){
        float score = 0;
        for (int det=0;det<myData->weights[w].details.size();det++){
            score += myData->mobiles[myData->i].details[det] * myData->weights[w].details[det];
        }
        score += myData->weights[w].bias;
        if (score > max) {
            max = score;
            whichIndex = w;
        }
    }
    if (whichIndex == myData->mobiles[myData->i].price_range) (myData->accuracy)++;
    pthread_exit(NULL);
}

void calculateAccuracy(vector<Weight> weights,vector<Mobile> mobiles){
    float accuracy = 0;

    pthread_t threads[mobiles.size()];

    struct thread_data threadData;
    threadData.mobiles = mobiles;
    threadData.weights = weights;
    threadData.accuracy = 0;
    for (long i=0;i<mobiles.size();i++) {
        threadData.i = i;
        int failed = pthread_create(&threads[i],NULL,calculateThread,(void *) &threadData);
        accuracy = threadData.accuracy;
    }
    accuracy /= mobiles.size();
    cout << "‫‪Accuracy:‬‬ " <<setprecision(2) << fixed << accuracy * 100 <<"%"<< endl;
    // pthread_exit(NULL);
}


int main(int argc,char* argv[]){
    vector<Weight> weights;
    vector<Mobile> mobiles;

    getCsvFiles(mobiles,weights,argv,1);
    getCsvFiles(mobiles,weights,argv,0);

    normilize(mobiles);
    calculateAccuracy(weights,mobiles);
}