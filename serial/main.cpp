#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <limits>
#include <iomanip>

using namespace std;

class Mobile {
    public:
        vector<float> details;
        float price_range;
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setPrice() {
            price_range = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }
};

class Weight{
    public:
        vector<float> details;
        float bias;
        void addDetail(float detail){
            details.push_back(detail);
        }
        void setBias() {
            bias = details[details.size()-1];
            details.erase(details.begin() + details.size()-1);
        }
};

void getCsvFiles(vector<Mobile> &mobiles,vector<Weight> &weights,
 char* argv[],int isWeight){
    fstream file;
    string dir = argv[1];
    string csvFile;
    string line;
    vector<string> lines;
    if (isWeight == 1) csvFile = "weights.csv";
    else csvFile = "train.csv";
    file.open(dir + string(csvFile));

    while(getline(file,line))
        lines.push_back(line);

    file.close();

    for (int each=1;each<lines.size();each++) {
        Weight w;
        Mobile m;
        vector<string> v;
        stringstream ss(lines[each]);

        while (ss.good()) {
            string substr;
            getline(ss, substr, ',');
            v.push_back(substr);
        }
        for (size_t i = 0; i < v.size(); i++)
            if (isWeight) w.addDetail(stof(v[i]));
            else m.addDetail(stof(v[i]));
        
        if (isWeight){
            w.setBias();
            weights.push_back(w);
        }
        else {
            m.setPrice();
            mobiles.push_back(m);
        }
    }
}

void normilize(vector<Mobile> &mobiles){
    int numberOfCols = mobiles[0].details.size();
    for (int eachCol=0;eachCol < numberOfCols;eachCol++){
        float min = numeric_limits<float>::max();
        float max = numeric_limits<float>::min();
        for (int i=0;i<mobiles.size();i++){
            if (mobiles[i].details[eachCol] < min)
                min = mobiles[i].details[eachCol];
            if (mobiles[i].details[eachCol] > max)
                max = mobiles[i].details[eachCol];
        }

        for (int i=0;i<mobiles.size();i++){
            float newX = (mobiles[i].details[eachCol] - min) / (max - min);
            mobiles[i].details[eachCol] = newX;
        }
    }
}

void calculateAccuracy(vector<Weight> weights,vector<Mobile> mobiles){
    float accuracy = 0;
    for (int i=0;i<mobiles.size();i++) {
        float max = numeric_limits<float>::min();
        int whichIndex;
        for (int w=0;w<weights.size();w++){
            float score = 0;

            for (int det=0;det<weights[w].details.size();det++)
                score += mobiles[i].details[det] * weights[w].details[det];

            score += weights[w].bias;
            if (score > max){
                whichIndex = w;
                max = score;
            }
        }
        if (whichIndex == mobiles[i].price_range) accuracy++;
    }
    accuracy /= mobiles.size();
    cout << "‫‪Accuracy:‬‬ " << setprecision(2) << fixed << accuracy * 100 <<"%"<< endl;
}


int main(int argc,char* argv[]){
    vector<Weight> weights;
    vector<Mobile> mobiles;

    getCsvFiles(mobiles,weights,argv,1);
    getCsvFiles(mobiles,weights,argv,0);

    normilize(mobiles);
    calculateAccuracy(weights,mobiles);
}